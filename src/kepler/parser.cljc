(ns kepler.parser
  "This namespace contains functions related to Mathyon parsing"
  (:require [clojure.walk :as walk]
            [clojure.string :as str]
            #?(:cljs [instaparse.core :as insta :refer-macros [defparser]]
               :clj [instaparse.core :as insta :refer [defparser]])
            [kepler.rules :refer [rule as-fn]]
            [kepler.quasiquote :refer [ex]]
            #?(:clj [criterium.core :as criterium])))


(def whitespace
  (insta/parser
    "whitespace = #'\\s*'"))

(defparser mathyon-s
  "math_script := assignment | factory
    term := (unaryterm (#'/|\\[x\\]|\\[/\\]' unaryterm)*)
    macroname := #'[a-zA-Z]+(_[a-zA-Z0-9]+)*'
    unaryterm := (#'-'? implicitterm)
    interval := #'(?!PI|EN)[A-Z][A-Z](?![A-Z])'
    editableangle := ('editableangle' '(' ')')
    tuple := ('(' ')') | ('(' expression (',' ')') | ((',' expression)+ ','? ')'))
    expression := (tuple | term | (#'\\+|\\-|\\[\\+-]' term) | finite_set (#'\\+|\\-|\\[\\+-]' term)*)
    finite_set := ('{' (','? '}') | ((expression ',')+ expression? '}'))
    charstring := ('\\'' #'[^\\']+' '\\'') | ('\"' #'[^\"]+' '\"')
    implicitterm := (power (#'\\*' power)*)
    power := (subscript ('**' | '^' subscript)*)
    symbol := #'[a-zA-Z\\$][a-zA-Z0-9\\']*'
    subscript := (factor ('_' factor)*)
    factor := entityvalue | editable | macro | ('{' parenexpression '}') | ('(' expression ')') | symbol | literal | charstring
    macro := (macroname '(' (relation (',' relation)* ','?)? ')')
    relation := (expression (#'==|<=|>=|!=|=|<|>' expression)*)
    angle := ('angle' | 'Angle' '(' point point point ')')
    point := #'[A-Z]'
    editabletriangle := ('editabletriangle' '(' ')')
    parenexpression := (expression)
    triangle := ('triangle' | 'Triangle' '(' point point point ')')
    literal := #'\\d+\\.\\d+( ?%?)|\\d+( ?%?)'
    polygon := ('polygon' | 'Polygon' '(' (point)* ')')
    editable := ('editable' '(' ')') | ('editable' '(' expression ')')
    entityvalue := polygon | angle | editableangle | interval | triangle | editabletriangle
    assignment := (symbol (',' symbol)* '=' assignmentrhs)
    assignmentrhs := factory | (expression (',' expression)*)
    factory := (#'[a-z_]+' '.' #'[a-z_]+' factoryparamlist)
    factoryparamlist := ('(' (factoryparam (',' factoryparam)* ','?)? ')')
    factoryparam := (symbol '=' _factoryarg) | _factoryarg
    _factoryarg := factory | expression"
  :auto-whitespace whitespace
)

(defparser mathyon
  "math_language := relation | expression
    term := (expression (('/'|'[x]'|'[/]') expression)*) | unaryterm | implicitterm | power
    macroname := #'[a-zA-Z]+(_[a-zA-Z0-9]+)*'
    unaryterm := ('-' expression)
    interval := #'(?!PI|EN)[A-Z][A-Z](?![A-Z])'
    editableangle := ('editableangle' '(' ')')
    tuple := ('(' ')') | ('(' expression (',' ')') | ((',' expression)+ ','? ')'))
    expression := term | (expression (('+'|'-'|'[+-]') expression)+) | tuple | factor | finite_set
    finite_set := ('{' (','? '}') | ((expression ',')+ expression? '}'))
    charstring := ('\\'' #'[^\\']+' '\\'') | ('\"' #'[^\"]+' '\"')
    implicitterm := (expression ('*' expression)+)
    power := (subscript (('**' | '^') subscript)+)
    symbol := #'[a-zA-Z\\$][a-zA-Z0-9\\']*'
    subscript := (factor ('_' factor)*)
    factor := entityvalue | editable | macro | parenexpression | bracketedexpression | symbol | literal | charstring
    bracketedexpression := '(' expression ')'
    macro := (macroname '(' (relation (',' relation)* ','?)? ')')
    relation := (expression (('=='|'<='|'>='|'!='|'='|'<'|'>') expression)*)
    angle := ('angle' | 'Angle' '(' point point point ')')
    point := #'[A-Z]'
    editabletriangle := ('editabletriangle' '(' ')')
    parenexpression := '{' expression '}'
    triangle := ('triangle' | 'Triangle' '(' point point point ')')
    literal := #'\\d+\\.\\d+( ?%?)|\\d+( ?%?)'
    polygon := ('polygon' | 'Polygon' '(' (point)* ')')
    editable := ('editable' '(' ')') | ('editable' '(' expression ')')
    entityvalue := polygon | angle | editableangle | interval | triangle | editabletriangle"
  :auto-whitespace whitespace
)

(defn op-of [expr] (if (coll? expr) (first expr)))
(defn args-of [expr] (if (coll? expr) (rest expr)))
(defn flatten-associative
  "Takes a nested associative tree, and unnests it.

  E.g. (+ (+ 2 3) 4) -> (+ 2 3 4)"
  [expression]
  (if-let [op (op-of expression)]
    (let [args (args-of expression)]
      (if (#{'+ '*} op)
        (let [new-args (for [arg args]
                         (if (= op (op-of arg))
                           (args-of arg)
                           [arg]))]
          (into [] (apply concat [op] new-args)))
        expression))
    expression))


(defn op [expr] (if (seq? expr) (first expr)))
(defn to-binary-tree [x & rest]
  (loop [expr x
         rest' rest]
    (if (not-empty rest')
      (let [[sym' y & rem] rest']
        (recur [(symbol sym') x y] rem))
      expr)))

(defn- str->int [s]
  #?(:clj (Integer/parseInt s)
     :cljs (js/parseInt s)))

(defn parse-mathyon
  "Parses the given Mathyon expression into a math expression tree."
  [expr]
  (->> (mathyon expr)
       (insta/transform
        {:literal (comp str->int str/trim)
         :subscript identity
         :power to-binary-tree
         :implicitterm to-binary-tree
         :term to-binary-tree
         :expression (comp flatten-associative to-binary-tree)
         :parenexpression (fn [_ expr _] expr)
         :bracketedexpression (fn [_ expr _] expr)
         :relation to-binary-tree
         :math_language identity
         :symbol symbol
         :factor identity})))


(parse-mathyon "10 - 3 - 4 - 5")
(parse-mathyon "10 + 3 + 4 + 5")
(parse-mathyon "x-1-2-3-4=10")
