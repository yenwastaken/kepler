(ns kepler.quasiquote
  "This namespace defines the `ex` macro, which behaves similarly to the `quote` macro,
  but allows unquoting. Unlike syntax quoting, quoted symbols are not namespace-qualified."
  (:refer-clojure :exclude [resolve]))

(defn error [message]
  #?(:clj (throw (Exception. message))
     :cljc (throw (js/Error. message))))

(def ^:dynamic *resolve*)

(def ^:dynamic ^:private *gensyms*)

(defn- resolve [sym]
  (let [ns (namespace sym)
        n (name sym)]
    (if (and (not ns) (= (last n) \#))
      (if-let [gs (@*gensyms* sym)]
        gs
        (let [gs (gensym (str (subs n 0 (dec (count n))) "__auto__"))]
          (swap! *gensyms* assoc sym gs)
          gs))
      (*resolve* sym))))

(defn unquote? [form]
  (and (seq? form) (= (first form) 'clojure.core/unquote)))

(defn unquote-splicing? [form]
  (and (seq? form) (= (first form) 'clojure.core/unquote-splicing)))

(defn- quote-fn* [form]
  (cond
    (symbol? form) `'~(resolve form)
    (unquote? form) (second form)
    (unquote-splicing? form) (error "splice not in list")
    (record? form) `'~form
    (coll? form)
      (let [xs (if (map? form) (apply concat form) form)
            parts (for [x xs]
                    (if (unquote-splicing? x)
                      (second x)
                      [(quote-fn* x)]))
            cat (doall `(concat ~@parts))]
        (cond
          (vector? form) `(vec ~cat)
          (map? form) `(apply hash-map ~cat)
          (set? form) `(set ~cat)
          (seq? form) `(apply list ~cat)
          :else (error "Unknown collection type")))
    :else `'~form))

(defn quote-fn [resolver form]
  (binding [*resolve* resolver
            *gensyms* (atom {})]
    (quote-fn* form)))

(defmacro defquote [name resolver]
  `(let [resolver# ~resolver]
     (defn ~(symbol (str name "-fn")) [form#]
       (quote-fn resolver# form#))
     (defmacro ~name [form#]
       (quote-fn resolver# form#))))

(defmacro ex [form]
  (quote-fn identity form))
