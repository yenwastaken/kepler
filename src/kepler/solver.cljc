(ns kepler.solver
  "Functions related to simplifying expressions and solving equations"
  (:require [clojure.core.reducers :as r]
            [clojure.walk :as walk]
            [clojure.set :as set]
            [clojure.zip :as zip]
            [clojure.string :as str]
            [taoensso.tufte :as tufte :refer (defnp p profiled profile)]
            #?@(:clj ([kepler.rules :refer [rule guard defruleset]]
                      [kepler.quasiquote :refer [ex]]
                      [clojure.math.numeric-tower :refer [expt exact-integer-sqrt]])
                :cljs ([kepler.rules :refer-macros [rule guard defruleset]]
                       [kepler.quasiquote :refer-macros [ex]]))))

(tufte/add-basic-println-handler! {})

(defn pow [base exp]
  #?(:clj (expt base exp)
     :cljs (Math/pow base exp)))

(defn integer-sqrt [n]
  #?(:clj (exact-integer-sqrt n)
     :cljs (let [sqrt (Math/floor (Math/sqrt n))]
             [sqrt (- n (pow sqrt 2))])))

#?(:cljs (def pmap map))
(defn nil-if-same
  "Decorator that ensures a function returns nil if its result is the same as its single argument"
  [func x]
  (if-let [x' (func x)]
    (if (= x' x)
      nil
      x')))

;; TODO: Numeric types should include fractions and decimals
(def numeric? integer?)
#_(defn neg [x]
  (cond
    (integer? x) (- x)
    (and (seqable? x) (= (first x) 'neg)) (second x)
    :else (ex' (neg ~x))))

#_(= (first (neg 'x)) '--)
#_(neg (neg 'x))
(defn nodes-used
  [expr]
  (cond
    (nil? expr)          #{}
    (integer? expr)      #{:int}
    (symbol? expr)       #{:sym}
    :else                 (let [[op & args] expr]
                            (reduce set/union #{op} (map nodes-used args)))))

(defn complexity
  "Returns a heuristic to determine the complexity of an expression."
  [expr]
  (do
    (cond
      (nil? expr)             0
      (numeric? expr)         1
      (symbol? expr)          1
      (keyword? expr)         1
      :else                   (let [[_ & args] expr]
                                (reduce + 1 (map complexity args))))))

(defn zipper-complexity
  "Returns a heuristic to determine the complxity of an expression zipper"
  [loc]
  (complexity (zip/root loc)))

(defn extract-nodes
  [pattern]
  (if (seqable? pattern)
    (apply set/union (map extract-nodes pattern))
    #{pattern}))

(extract-nodes '[= 0 [** x 2] [* 2 x] 1])
(extract-nodes '(+ 1 2 (- 3 x)))

(defn apply-all [rules expr]
  (let [expr-nodes (extract-nodes expr)]
    (->> rules
         (filter #(set/subset? (-> % meta :constants) expr-nodes))
         (map #(% expr))
         (remove nil?))))

(defn test-ruleset [rules expr] (apply-all rules expr))
(defn neg [x]
  (if (number? x)
    (- x)
    (list '- x)))
(defruleset negation-rules
  [(rule '(- (- ?x))     :==> '?x)
   (rule '(- ?x)         :==> (ex ~(- ?x)) :if (guard (numeric? ?x)))
   (rule '(- (+ ?&+))    :==> (ex (+ ~(map #(neg %) ?&+))))
   (rule '(- ?x (- ?y))  :==> '(+ ?x ?y))
   (rule '(- (* ?x ?&*)) :==> (ex (* ~(neg ?x) ~@?&*)))])

(time (test-ruleset negation-rules '(- (* 1 2 4))))
(time (test-ruleset negation-rules '(- (+ 1 2 4))))

(defruleset add-sub-rules
  [(rule '(+ ?x)              :==> '?x)
   (rule '(+)                 :==> 0)
   (rule '(- (+ ?&+) ?y)      :==> (ex (+ ~(map #(neg %) ?&+) ~(neg ?y))))
   (rule '(+ (+ ?&+1) ?&+2)   :==> (ex (+ ~@?&+1 ~@?&+2)))
   (rule '(+ ?x (- ?x))       :==> 0)
   (rule '(+ ?x (- ?x) ?&+)   :==> (ex (+ ~@?&+)))
   (rule '(- ?x ?x)           :==> 0)
   (rule '(+ (- ?x ?y) ?&*)   :==> (ex (+ ~?x ~(neg ?y) ~@?&*)))])

(time (test-ruleset add-sub-rules '(- (+ 295 x) x)))
(time (test-ruleset add-sub-rules '(+ x (- x))))
(time (test-ruleset add-sub-rules '(+ x (- x y))))

(defn reduce-nums
  "Given an expression, reduces the numerals with the given operation while retaining
  the non-numerals"
  [op sym args]
  (let [ints        (filter numeric? args)
        non-ints    (filter (complement numeric?) args)
        reduced     (reduce op ints)]
    (if (> (count ints) 1)
      (if (> (count non-ints) 0)
        (concat [sym] non-ints [reduced])
        reduced))))

(def add-nums (partial reduce-nums + '+))
(def mul-nums (partial reduce-nums * '*))

(defruleset integer-add-rules
  [(rule '(+ ?x)  :==> '?x)
   (rule '(+)     :==> 0)
   (rule '(+ ?&+) :==> (add-nums ?&+))])

(time (test-ruleset integer-add-rules '[+ x y 4 4 4]))

(defruleset add-like-terms-rules
  [(rule '(+ ?x ?x ?&*)               :==> (ex (+ (* 2 ~?x) ~@?&*)))
   (rule '(+ (* ?n ?x) ?x ?&*)        :==> (ex (+ (* (+ ~?n 1) ~?x) ~@?&*)))
   (rule '(+ (* ?n ?x) (* ?m ?x) ?&*) :==> (ex (+ (* (+ ~?n ~?m) ~?x) ~@?&*)))])

(test-ruleset add-like-terms-rules '(+ (* 2 x) 6 (* 3 x)))
(time (test-ruleset add-like-terms-rules '(+ y y)))

(defruleset integer-sub-rules
  [(rule '(-)       :==> 0)
   (rule '(- ?x)    :==> (neg ?x) :if (guard (numeric? ?x)))
   (rule '(- ?x ?y) :==> (- ?x ?y) :if (guard (and (numeric? ?x) (numeric? ?y))))])

(time (test-ruleset integer-sub-rules '(- 1 2)))

(defruleset integer-mul-rules
  [(rule '(* ?x)        :==> ?x)
   (rule '(*)           :==> 1)
   (rule '(* ?&+)       :==> (mul-nums ?&+))])

(time (test-ruleset integer-mul-rules '(* 1 2 y 3 4 x)))
(time (test-ruleset integer-mul-rules '(* 1)))
(time (test-ruleset integer-mul-rules '(*)))

(defn gcd [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defruleset integer-div-rules
  [(rule '(/)       :==> 1)
   (rule '(/ ?x 1)  :==> '?x)
   (rule '(/ ?x ?y) :==> (/ ?x ?y) :if (guard (and
                                               (numeric? ?x)
                                               (numeric? ?y)
                                               (not= 0 ?y)
                                               (numeric? (/ ?x ?y)))))
   (rule '(/ ?x ?y) :==>
         (let [d (gcd ?x ?y)]
           ['/ (/ ?x d) (/ ?y d)])
         :if (guard (and (numeric? ?x)
                         (numeric? ?y)
                         (not= 0 ?y)
                         (not= (gcd ?x ?y) 1))))])

(time (test-ruleset integer-div-rules '(/ 6 8)))
(defruleset integer-exp-rules
  [(rule '(** ?x ?y) :==> (pow ?x ?y) :if (guard (and (numeric? ?x) (numeric? ?y) (not= 0 ?y))))])

(time (test-ruleset integer-exp-rules '(** 3 4)))
(time (test-ruleset integer-exp-rules '(** 3 x)))

(defruleset multiplication-rules
  [(rule (ex (* ?x ?x))         :==> (ex (** ?x 2)))
   (rule (ex (* (* ?&*1) ?&*2)) :==> (ex (* ~@?&*1 ~@?&*2)))])

(time (test-ruleset multiplication-rules '(* 2 (* 3 x))))
(time (test-ruleset multiplication-rules '(* (* 3 x) (* 3 x))))

;; (defruleset addition-identity-rules
;;   [(rule (ex (+ ?x 0)) :=> ?x)])

;; (defruleset addition-associativity-rules
;;   [(rule (ex (+ ?x)) :=> ?x)
;;    (rule (ex (+)) :=> 0)
;;    (rule (ex (+ (+ ?&*) ?&*2)) :=> (ex (+ ?&* ?&*2)))])

;; (test-ruleset addition-associativity-rules (ex (+ (+ (* 5 8) (* 5 x) (* x 8) (* x x)) 2)))
(defruleset plus-minus-rules
  [(rule '(+-) :==> 0)
   (rule '(+- (- ?x)) :==> (ex (+- ~?x)))])

(defruleset plus-minus-identity-rules
  [(rule '(+- ?x 0) :==> ?x)])

(defruleset expand-set-rules
  [(rule '(+ ?x (set ?&*)) :==> (list* 'set (map #(list '+ ?x %) ?&*)))
   (rule '(- ?x (set ?&*)) :==> (list* 'set (map #(list '- ?x %) ?&*)))
   (rule '(* ?x (set ?&*)) :==> (list* 'set (map #(list '* ?x %) ?&*)))
   (rule '(/ (set ?&*) ?x) :==> (list* 'set (map #(list '/ % ?x) ?&*)))])

(list* 'set (map #(list '/ % 2) '(4 2)))
(test-ruleset expand-set-rules '(/ (set 2 4) 2))
;; (transform-one-level expand-set-rules (ex (* (set 1 2) 3)))
(defruleset expand-plus-minus
   [(rule '(+- ?x ?y) :==> '(set (+ ?x ?y) (- ?x ?y) )
                 :if (guard (and (number? ?x)
                                 (number? ?y))))])

(defn first-of [& rules]
  (fn [expr] (->> rules
                  (map #(% expr))
                  (remove nil?)
                  first)))

(defn term [var]
  (first-of
   (rule (ex (* (** ~var ?n) ?a)) :=> '{:coeff ?a, :power ?n} :if (guard (integer? ?n)))
   (rule (ex (** ~var ?n))        :=> '{:coeff 1, :power ?n} :if (guard (integer? ?n)))
   (rule (ex (* ~var ?a))         :=> '{:coeff ?a, :power 1})
   (rule var                      :=> '{:coeff 1, :power 1})
   (rule '?x                      :=> '{:coeff ?x, :power 0} :if (guard (numeric? ?x)))))

(defn to-polynomial [var]
  (rule '(= 0 (+ ?&+terms)) :==> (let [terms (map (comp :result (term var)) ?&+terms)]
                                   (into [] (sort-by (comp - :power) terms)))
        :if (guard (every? (term var) ?&+terms))))

(kepler.pattern-match/match-one '(+ ?&+terms) '(+ x (* 2 x)) (guard (every? (term 'x) ?&+terms)))
(time ((to-polynomial 'x) '(= 0 (+ x (* (** x 2) 2) 80))))
(time (->> ((term 'y) '(* 10 (** y 9)))
           :result))
;; (transform-one-level expand-set-rules (ex (/ (set 2 3) 4)))

;; (transform-one-level plus-minus-rules (ex (+- 1 2)))
(defruleset sqrt-rules
  [(rule '(sqrt ?x) :==> (first (integer-sqrt ?x))
         :if (guard (and
                     (numeric? ?x)
                     (<= 0 ?x)
                     (= 0 (second (integer-sqrt ?x))))))])


(test-ruleset sqrt-rules '(sqrt 16))
(defruleset quadratic-formula-rules
  [(rule '(= (+ (* ?a (** ?x 2)) (* ?b ?x) ?c) 0) :==>
         (ex (= ~?x (/ (+- ~(neg ?b) (sqrt (- (** ~?b 2) (* 4 ?a ~?c))))
                       (* 2 ~?a))))
         :if (guard (and (numeric? ?a)
                         (numeric? ?b)
                         (numeric? ?c))))
   (rule '(= (+ (** ?x 2) (* ?b ?x) ?c) 0) :==>
         (ex (= ~?x (/ (+- ~(neg ?b) (sqrt (- (** ~?b 2) (* 4 1 ~?c))))
                       (* 2 1))))
         :if (guard (numeric? ?b)))])

(defruleset equation-inverse-rules
  [(rule '(= ?x ?y) :=> '(= (- ?x ?y) 0) :if (guard (and (not= ?x 0)
                                                         (not= ?y 0))))
   #_(rule '(= (+ ?x ?y) 0) :==> (ex (= ~?x ~(neg ?y))) :if (guard (symbol? ?x)))
   (rule '(= (+ ?x ?y) 0) :=> '(= ?x (- ?y)) :if (guard (symbol? ?x)))
   (rule '(= (- ?x ?y) 0) :=> '(= ?x ?y) :if (guard (symbol? ?x)))
   (rule '(= (** ?x 2) ?y) :=> '(= ?x (sqrt ?y)))])

(time (test-ruleset equation-inverse-rules '(= x (+ x 100))))

(time (test-ruleset equation-inverse-rules '(= 0 (+ x 3))))

(defruleset completion-rules
  [(rule '?x :=> :done :if (guard (or (symbol? ?x)
                                      (number? ?y))))
   (rule '(= ?x ?y) :=> :done
         :if (guard (and (symbol? ?x)
                         (numeric? ?y))))
   (rule '(= ?x (set ?&+)) :=> :done
         :if (guard (every? numeric? ?&+)))])

(defruleset inverse-rules
  [(rule '(- (- ?x ?y) ?z)    :==> (add-nums [?x (neg ?y) (neg ?z)]))
   (rule '(- ?x (- ?y ?z))    :==> (add-nums [?x ?y (neg ?z)]))
   (rule '(- (+ ?&+))         :==> (add-nums (vec (map #(neg %) ?&+))))
   (rule '(- ?x ?&+)          :==> (add-nums (list* ?x (map #(neg %) ?&+))))
   (rule '(- (* ?&+))         :==> (mul-nums (list* -1 ?&+)))])

(time (test-ruleset inverse-rules '(- (- (- 1 2) 3) 4)))
(time (test-ruleset inverse-rules '(+ (- 1 y) x)))
(time (test-ruleset inverse-rules '(- (- 1 2))))
(time (test-ruleset inverse-rules '(- (+ 1 2 3 x))))
(time (test-ruleset inverse-rules '[- [* 1 2 3 x]]))
(time (test-ruleset completion-rules '(= x -10)))
(time (test-ruleset quadratic-formula-rules '(= (+ (** x 2) (* 3 x) (* b b)) 0)))
(time (test-ruleset inverse-rules '[- [* 4 x]]))

(defn complexity-compare
  [x y]
  (let [x' (:to x)
        y' (:to y)
        c (compare (complexity x') (complexity y'))]
    (if (not= c 0)
      c
      (compare (str x') (str y')))))

(defn- vec-conj [coll val]
  (if (nil? coll) [] (conj coll val)))

(defn- conj-in [map key val] (update map key #(vec-conj % val)))

(defn search
  "Given a starting node and a generator function (of type node -> [edge]),
  return a lazy sequence of edges."
  [edge-fn s]
  ((fn rec-bfs
     [explored frontier]
     (lazy-seq
      (if (empty? frontier)
        nil
        (let [edge       (first frontier)
              node       (:to edge)
              new-edges  (->> (edge-fn node)
                              (remove (comp explored :to))
                              (map #(assoc % :path (vec-conj (:path edge) %))))
              new-nodes  (map :to new-edges)]
          #_(println (pr-str "Edge: " edge))
          #_(println (pr-str "Node: " node))
          #_(println (str "New-edges: " (into [] new-edges)))
          #_(println (str "New-nodes: " (into [] new-nodes)))
          (cons edge (rec-bfs
                      (into explored new-nodes)
                      (into (set/difference frontier #{edge}) new-edges)
                      #_(into (rest frontier) (remove (comp explored :to) new-edges))))))))
   #{s} (into (sorted-set-by complexity-compare)
              (remove (comp #{s} :to)
                      (map #(assoc % :path [%]) (edge-fn s))))))


(defn take-until
  "Returns a lazy sequence of successive items from coll until
   (pred item) returns true, including that item. pred must be
   free of side-effects."
  [pred coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (if (pred (first s))
       (cons (first s) nil)
       (cons (first s) (take-until pred (rest s)))))))


;; loc -> loc
(defn topmost
  "Given a zipper loc, returns the root-level loc"
  [loc]
  (if-let [parent (zip/up loc)]
    (recur parent)
    loc))


(defn zipper-dfs [z]
  (->> z
       (iterate zip/next)
       (take-while (complement zip/end?))
       (filter #(seqable? (zip/node %)))))

(->> '[= 0 [+ [** x 2] [- [* 4 x]] 3]]
     zip/vector-zip
     zipper-dfs)

(defn try-apply-to-node
  "rules -> loc -> maybe loc"
  [func loc]
  (when-let [result (func (zip/node loc))]
    (zip/replace loc result)))


(defn try-apply-ruleset-to-node
  "rules -> (loc -> maybe loc)"
  [ruleset loc]
  (when-let [result (first (apply-all ruleset (zip/node loc)))]
    {:from loc
     :to (zip/replace loc (:result result))
     :rule (:rule result)
     :matches (:matches result)}))


(defn try-apply-rulesets
  [rulesets loc]
  (->> rulesets
       (map #(try-apply-ruleset-to-node % loc))
       (remove nil?)))

(def all-rules
  [#_multiply-out-rules
   #_addition-identity-rules
   #_addition-associativity-rules
   integer-add-rules
   negation-rules
   add-like-terms-rules
   add-sub-rules
   integer-sub-rules
   integer-mul-rules
   integer-div-rules
   integer-exp-rules
   inverse-rules
   multiplication-rules
   plus-minus-rules
   plus-minus-identity-rules
   quadratic-formula-rules
   sqrt-rules
   expand-plus-minus
   expand-set-rules
   equation-inverse-rules
   completion-rules])

(defn to-zipper [v]
  (cond
    (vector? v) (zip/vector-zip v)
    :else       (zip/seq-zip v)))

(defn print-and-pass [x] (println x) x)
(defn find-next-steps
  [rulesets expr]
  (->> expr
       to-zipper
       zipper-dfs 
       (pmap (partial try-apply-rulesets rulesets))
       (apply concat)
       (remove nil?)
       (map (fn [{:keys [from to rule matches]}]
              {:from expr
               :from-context from
               :to (zip/root to)
               :to-context to
               :rule rule
               :matches matches}))))

(->> (find-next-steps [integer-mul-rules] '(whatever (* 2 3 5)))
     (map #(select-keys % [:from :to :matches])))

(defn mark-as
  [sym loc]
  (let [node (zip/node loc)]
    (zip/replace loc (list sym node))))

(def mark-as-highlighted (partial mark-as :highlighted))
(def mark-as-changing (partial mark-as 'changing))
(def mark-as-changed (partial mark-as 'changed))


(declare stringify-child)
(defn surround-with [start end s] (str start s end))
(def surround-brackets (partial surround-with "(" ")"))
(def surround-braces (partial surround-with "{" "}"))
(def surround-spaces (partial surround-with " " " "))
(def highlight
  #?(:clj (partial surround-with "\u001b[36;4m" "\u001b[0m")
     :cljs identity))
(defn op-precedence [op]
  (case op
    + 1
    - 1
    * 2
    / 3
    set 2
    ** 2
    = 4
    > 4
    < 4
    >= 4
    <= 4
    0))

(defn op-of [expr]
  (if (seqable? expr)
    (let [[op & args] expr]
      (if (= op :highlighted)
        (op-of (first args))
        op))))

(defn stringify-op [op]
  (case op
    +-    "[+-]"
    neg   "-"
    (str op)))

(defn stringify-expr [expr]
  (cond
    (number? expr) (str expr)
    (symbol? expr) (str expr)
    (keyword? expr) (str expr)
    (= (first expr) :highlighted) (highlight (stringify-expr (second expr)))
    (= (first expr) 'set) (surround-braces (str/join ", " (map stringify-expr (rest expr))))
    (seqable? expr)    (let [[op & args] expr]
                     (if (= (count args) 1)
                       (str (stringify-op op) (surround-brackets (str/join "" (map stringify-expr args))))
                       (str/join (surround-spaces (stringify-op op)) (map (stringify-child expr) (rest expr)))))))

(defn stringify-child [parent]
  (fn [child]
    (cond
      (symbol? child)      (stringify-expr child)
      (number? child)      (stringify-expr child)
      (= '= (first parent)) (stringify-expr child)
      :else                (let [parent-op (op-of parent)
                                 child-op  (op-of child)]
                             (if (<= (op-precedence parent-op) (op-precedence child-op))
                               (stringify-expr child)
                               (surround-brackets (stringify-expr child)))))))

(defn test-rule [rule expr] (rule expr))
#_(defn extract-vars-for-quadratic
  [expr]
  (let [rules [(rule (ex (= (+ (* ?a (** ?x 2)) (* ?b ?x) ?c) 0)) :=> (ex [?a ?b ?c]))
               (rule (ex (= (+ (** ?x 2) (* ?b ?x) ?c) 0)) :=> (ex [1 ?b ?c]))]]
    (->> rules
         (map #(apply-rule % expr))
         (remove nil?)
         first)))

#_(defn extract-vars-from-add [expr] (apply-rule (rule (ex (+ ?&*)) :=> [?&*]) expr))
#_(defn extract-vars-from-sub
  [expr]
  (let [rules [(rule (ex (- ?y)) :=> [0 ?y])
               (rule (ex (- ?x ?y)) :=> [?x ?y])]]
    (->> rules
         (map #(apply-rule % expr))
         (remove nil?)
         first)))

#_(defn extract-vars-from-mul [expr] (apply-rule (rule (ex (* ?&*)) :=> [?&*]) expr))
#_(defn extract-vars-from-div [expr] (apply-rule (rule (ex (/ ?&*)) :=> [?&*]) expr))
#_(defn extract-vars-from-pow [expr] (apply-rule (rule (ex (** ?x ?y)) :=> [?x ?y]) expr))
#_(defn extract-vars-from-sqrt [expr] (apply-rule (rule (ex (sqrt ?x)) :=> ?x) expr))
#_(defn and-join
  [& xs] (case (count xs)
           0 ""
           1 (str (first xs))
           (str (str/join ", " (butlast xs)) " and " (last xs))))

#_(defn hint
  [ruleset from to]
  (case ruleset
    integer-add-rules        (let [xs (extract-vars-from-add from)
                                   y to]
                               (str "Add " (apply and-join (filter numeric? xs))
                                    " to get " (stringify-expr y))),
    integer-sub-rules        (let [[x y] (extract-vars-from-sub from)
                                   z to]
                               (str "Subtract " y " from " x " to get " (stringify-expr z))),
    integer-mul-rules        (let [xs (extract-vars-from-mul from)
                                   y to]
                               (str "Multiply " (apply and-join (filter numeric? xs))
                                    " to get " (stringify-expr y))),
    integer-div-rules        (let [[x y] (extract-vars-from-div from)]
                               (str "div " x " by " y " to get " (stringify-expr to))),
    addition-identity-rules  "0 plus anything results in the same thing"
    add-like-terms-rules     "Like terms can be combined"
    integer-exp-rules        (let [[base exp] (extract-vars-from-pow from)]
                               (str (stringify-expr base) " to the power of "
                                    (stringify-expr exp) " is "
                                    (stringify-expr to)))
    sqrt-rules               (let [x (extract-vars-from-sqrt from)]
                               (str "Square root of " (stringify-expr x) " is " (stringify-expr to)))
    quadratic-formula-rules  (let [[a b c] (extract-vars-for-quadratic from)]
                               (str "Substitute a = " (stringify-expr a)
                                    ", b = " (stringify-expr b)
                                    " and c = " (stringify-expr c) " into the quadratic formula"))
    expand-plus-minus        (str "Plus-minus can be split into a plus expression "
                                  "and a minus expression")
    plus-minus-identity-rules "Anything plus or minus 0 results in the same thing"
    (str "No hints defined for " (str ruleset) " yet :/")))


(defn print-edge
  [edge]
  (let [from-context (->> edge :from-context)
        from (->> edge :from)
        to-context (->> edge :to-context)
        to   (->> edge :to)]
        (str #_(str "Step:     "  (stringify-expr from) "\n")
             (str "Step     : " (stringify-expr (-> from-context mark-as-highlighted zip/root)) "\n")
             #_(str "From     : " (pr-str (-> from-context mark-as-highlighted zip/root))
                                #_#_" Complexity: " (complexity from) "\n")
             #_(str "Hint     : " (hint (->> edge :rule) (zip/node from-context) (zip/node to-context)) "\n")
             (str "Rule     : " (->> edge :rule) "\n")
             (str "Matches  : " (->> edge :matches) "\n")
             #_(str "To       : " (pr-str (-> to-context mark-as-highlighted zip/root))
                                #_#_" Complexity: " (complexity to) "\n")
             (str "Next step: "  (stringify-expr (-> to-context mark-as-highlighted zip/root)) "\n")
             "-------------------------")))

(defn is-simplest [expr] (= expr :done))

(defn simplify [expr]
  (->> (search (partial find-next-steps all-rules) (to-zipper expr))
       (take-until #(is-simplest (zip/root (:to %))))))

(defn solve
  [expr]
  (->> expr
       (search (partial find-next-steps all-rules))
       (take-until #(is-simplest (:to %)))
       (take 100)
       (apply min-key (comp complexity :to))
       :path
       #_count))

(defn test-expr
  [expr]
  (do
    (println "Solving: " (stringify-expr expr))
    (println "======================================================")
    (time (->> (solve expr)
               (map print-edge)
               (map println)))))

(defn expression? [expr] (seqable? expr))
(defn leaf? [expr] (not (expression? expr)))
(defn commutative? [op] (contains? #{'+ '* '=} op))

(defn node-type-order [expr]
  (cond
    (symbol? expr) 0
    (number? expr) 1
    :else          2))

(defn try-vec! [x] (if (seqable? x) (vec x) x))
(compare (vec '(1 2 3)) (vec '(4 5)))
(defn expr-compare [expr1 expr2]
  (let [[node-type-1 node-type-2] (map node-type-order [expr1 expr2])
        node-comp (compare node-type-1 node-type-2)]
    (if (= 0 node-comp)
      (compare (try-vec! expr1) (try-vec! expr2))
      node-comp)))

(defn sort-expr [expr]
  (if (leaf? expr)
    expr
    (let [[op & args] expr]
      (if (commutative? op)
        (list* op (sort expr-compare (map sort-expr args)))
        expr))))

(defn canonical [expr] (sort-expr expr))


(defn tree-eq [ctree-a ctree-b] (= ctree-a ctree-b))

(tree-eq (canonical '(* 1 (+ 7 2) (- 4 3) x 56)) (canonical '(* 1 56 (+ 2 7) x (- 4 3))))

(defn apply-sequential [rules expr]
  (loop [rules' rules
         expr' expr
         accum []]
    (if-let [r (first rules')]
      (if-let [next-expr (r expr')]
        (recur (rest rules') (:result next-expr) (conj accum next-expr))
        accum)
      accum)))


(comment
  (test-expr (kepler.parser/parse-mathyon "x**2 + 2*x + 1 = 0")
  (test-expr (kepler.parser/parse-mathyon "x - 1 - 2 - 3 - 4 = 0"))
  (test-expr '(= 0 (+ x 1 2)))
  (test-expr '(= 0 (+ (** x 2) (- (* 4 x)) 3)))
  (test-expr '(= (+ (* 10 x) 900 (* 20 x)) 0))
  (test-expr '[= 0 [+ [** x 2] [- [* 4 x]] 3]])
  (test-expr '[= 0 [+ [** x 2] [* 1 x] 1]])))
