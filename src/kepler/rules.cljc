(ns kepler.rules
  "Defines the `rule` macro and friends.

  A 'rule' has the following shape:

  (rule <pattern> :==> <transform-fn> (:if <guard-fn>)?

  It defines a function that takes any expression, attempts to match it against the given pattern, and applies
  the bindings to the transform function.
  "
  (:require [kepler.pattern-match :refer [pattern-variables match-one variable?]]
            [clojure.walk :as walk]
            [clojure.set :as set]))


(defn extract-constants [pattern]
  (cond
    (variable? pattern) nil
    (seqable? pattern)      (reduce set/union (map extract-constants pattern))
    :else               #{pattern}))

(defn ^:private build-substitution-function
  "Returns a function that attempts to pattern-match an expression, and if successful,
  substitutes the values into a different expression"
  ([pattern to]
   `(fn [expr#]
      (if-let [bindings# (match-one ~pattern expr#)]
        {:rule [~pattern :=> ~to]
         :matches bindings#
         :result (walk/postwalk-replace bindings# ~to)})))
  ([pattern to guard]
   `(fn [expr#]
     (if-let [bindings# (match-one ~pattern expr# ~guard)]
       {:rule [~pattern :=> ~to :if '~guard]
        :matches bindings#
        :result (walk/postwalk-replace bindings# ~to)}))))


(defn ^:private as-fn*
  [form]
  (let [vars (vec (pattern-variables form))
        new-vars (into {} (zipmap vars (map gensym vars)))
        bindings' (gensym)]
    `(fn [~bindings']
       (let ~(into [] (mapcat (fn [v] [(new-vars v) `(get ~bindings' '~v)]) vars))
         ~(walk/postwalk-replace new-vars form)))))

(defmacro as-fn
  [form]
  (as-fn* form))

(defn ^:private build-transformation-function
  "Returns a function that attempts to pattern-match an expression, and if successful,
  calls the passed-in function with the bindings as arguments"
([pattern form]
   `(fn [expr#]
      (if-let [bindings# (match-one ~pattern expr#)]
        (if-let [result# (~(as-fn* form) bindings#)]
          {:rule [~pattern :==> '~form]
           :matches bindings#
           :result result#}))))
  ([pattern form guard]
   `(fn [expr#]
      (if-let [bindings# (match-one ~pattern expr# ~guard)]
        (if-let [result# (~(as-fn* form) bindings#)]
          {:rule [~pattern :==> '~form :if '~guard]
           :matches bindings#
           :result result#})))))

(defn ^:private rule-fn
  ([pattern trans to]
   `(with-meta
      ~(case trans
         :=>  (build-substitution-function pattern to)
         :==> (build-transformation-function pattern to))
      {:constants (extract-constants ~pattern)}))
  ([pattern trans to _ guard]
   `(with-meta
      ~(case trans
         :=>  (build-substitution-function pattern to guard)
         :==> (build-transformation-function pattern to guard))
      {:constants (extract-constants ~pattern)})))

;; In the future we might want to optimize the guard functions
;; As such we'll denote it separately from as-fn
(defmacro guard [form] (as-fn* form))

(defmacro rule [& args] (apply rule-fn args))
(defn- merge-meta [obj m] (with-meta obj (merge (meta obj) m)))
(defn- update-rule-meta
  [name rule']
  (merge-meta rule' {:name name}))

(defn ruleset
  [name rules]
  (->> rules
       (map (partial update-rule-meta name))))

(defmacro defruleset
  [name rules]
  `(def ~name (with-meta (ruleset '~name ~rules) {:name '~name})))
