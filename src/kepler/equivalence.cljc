(ns kepler.equivalence
  "Functions related to evaluating equivalences between expressions"
  (:require [clojure.core.reducers :as r]
            [clojure.walk :as walk]
            [clojure.set :as set]
            [clojure.zip :as zip]
            [clojure.string :as str]
            [taoensso.tufte :as tufte :refer (defnp p profiled profile)]
            #?@(:clj ([kepler.rules :refer [rule guard defruleset]]
                      [kepler.quasiquote :refer [ex]]
                      [clojure.math.numeric-tower :refer [expt exact-integer-sqrt]])
                :cljs ([kepler.rules :refer-macros [rule guard defruleset]]
                       [kepler.quasiquote :refer-macros [ex]]))))

(defn nodes-used
  [expr]
  (cond
    (nil? expr)          #{}
    (integer? expr)      #{expr}
    (symbol? expr)       #{expr}
    :else                 (let [[op & args] expr]
                            (reduce set/union #{op} (map nodes-used args)))))
(def any? (complement not-any?))
(defn expression? [expr] (seqable? expr))
(defn leaf? [expr] (not (expression? expr)))
(defn commutative? [op] (contains? #{'+ '* '=} op))
(defn variable? [expr] (and (symbol? expr)
                            (re-seq #"[a-zA-Z]+" (name expr))))
(defn term? [expr] (any? variable? (nodes-used expr)))
(defn node-type-order [expr]
  (cond
    (term? expr)   0
    (symbol? expr) 0
    (number? expr) 1
    :else          2))

(defn try-vec! [x] (if (seqable? x) (vec x) x))
(defn expr-compare [expr1 expr2]
  (let [[node-type-1 node-type-2] (map node-type-order [expr1 expr2])
        node-comp (compare node-type-1 node-type-2)]
    (if (= 0 node-comp)
      (compare (try-vec! expr1) (try-vec! expr2))
      node-comp)))

(defn sort-expr [expr]
  (if (leaf? expr)
    expr
    (let [[op & args] expr]
      (if (commutative? op)
        (list* op (sort expr-compare (map sort-expr args)))
        expr))))

(defn canonical-sort [expr] (sort-expr expr))

(canonical-sort '[+ 1 3 2 [* 2 x] 5])


(defn strict-equivalent
  [expr1 expr2]
  (= (canonical-sort expr1)
     (canonical-sort expr2)))
