(ns kepler.app
  (:require [reagent.core :as r]
            [kepler.rules :refer [rule as-fn]]
            [kepler.quasiquote :refer [ex]]
            [kepler.solver :as solver]
            [kepler.parser :refer [parse-mathyon]]))

(enable-console-print!)

;; define your app data so that it doesn't get over-written on reload

(def test-rule (rule '(+ ?x ?x) :==> (as-fn (* (+ 2 2)))))
(defonce app-state (r/atom {:mathyon-input ""
                            :steps []}))

(defn edge-component [key {:keys [from rule matches to]}]
  ^{:key key} [:div
   #_[:p (str "From: " (solver/stringify-expr from))]
               [:p (str "rule   : " rule)]
               #_[:p (str "matches: " matches)]
               [:p (str "-> " (solver/stringify-expr to))]
               [:hr]])

(defn set-mathyon!
  [mathyon]
  (swap! app-state assoc :mathyon-input mathyon))

(defn trigger-solve!
  [mathyon]
  (let [steps (-> mathyon parse-mathyon solver/solve)]
    (swap! app-state assoc :steps steps)))

(defn root []
  [:div
   "Math expression: "
   [:input {:type "text"
            :value (:mathyon-input @app-state)
            :on-change #(set-mathyon! (-> % .-target .-value))}]
   [:button {:type "button"
            :class "btn btn-default"
             :on-click #(trigger-solve! (:mathyon-input @app-state))}
    "Solve!"]
   (map-indexed (fn [idx edge] (edge-component idx edge)) (:steps @app-state))])

(r/render [root] (js/document.getElementById "app"))
(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
