(ns kepler.pattern-match
  "This namespace implements functions for pattern-matching. Pattern matching allows us to define
  transformation rules in a declarative manner.
  For example, we can define a rule that transforms (x + x) to 2x as follows:

  (rule '(+ ?x ?x) :=> '(* 2 ?x))

  Pattern matching will allow us to
  - determine if a given pattern can be applied to an expression
  - extract variables from said expression.

  (match-one '(+ ?x ?x) '(+ 6 6)) ;; this gives {?x 6}

  A pattern is either a constant, a pattern variable, or a sequence of patterns. A
  pattern variable is any symbol starting with '?'.

  The difference between this and core.match is that we need to support commutative
  pattern matching - e.g. (+ ?x 10) should match (+ 10 20) with ?x matched with 20.
  Commutative pattern matching is known to be an NP-hard problem, but we can mitigate
  some of the performance issues with aggressive tree pruning.
  "
  (:require [clojure.math.combinatorics :as combo]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.test :as test :refer [deftest]]
            [clojure.walk :as walk]
            #?(:clj [criterium.core :as criterium])
            #?(:clj [clojure.tools.trace :as trace :refer [deftrace]])))

;; Symbols starting with a "?" are free variables - they can be matched to anything.
(defn symbol-prefixed-with
  [prefix x]
  (and (symbol? x)
       (str/starts-with? (name x) prefix)))

(defn variable? [x] (symbol-prefixed-with "?" x))

(deftest test-variable?
  (test/is (variable? '?x))
  (test/is (variable? 'user/?x))
  (test/is (not (variable? 5))))

;; Symbols starting with ?&* or ?&+ are sequence variables - they can match a sequence of
;; arguments. ?&* matches 0 or many elements, ?&+ matches 1 or many. 
(defn seq-star-variable? [x] (symbol-prefixed-with "?&*" x))
(defn seq-plus-variable? [x] (symbol-prefixed-with "?&+" x))

(deftest test-seq-star-variable?
  (test/is (seq-star-variable? '?&*))
  (test/is (not (seq-star-variable? '?&+)))
  (test/is (not (seq-star-variable? '?x))))

(deftest test-seq-plus-variable?
  (test/is (seq-plus-variable? '?&+))
  (test/is (not (seq-plus-variable? '?&*)))
  (test/is (not (seq-plus-variable? '?x))))

(defn expression? [expr] (seqable? expr))
(defn commutative? [op] (contains? #{'+ '* '=} op))
(defn op-of [expr] (if (expression? expr) (first expr) nil))


;; A common operation we'll need to do is to take two binding maps and attempt to merge them.
;; E.g. {?x 1, ?y 7} can be merged with {?z 10} to form {?x 1, ?y 7, ?z 10}
;; However, we will want that merge to fail if there's a conflict between any common key
;; E.g {?x 1, ?y 7} should *not* be mergeable with {?x 7, ?z 10}, because the values of ?x are different.
(defn merge-unique
  [& maps]
  "Merges all the maps. Returns nil if any shared key between any pair of maps has differing values."
  (if (not-any? nil? maps)
    (let [setify-values (fn [m] (into {} (map (fn [[k v]] [k #{v}]) m)))
          merged (->> maps
                      (map setify-values)
                      (apply merge-with set/union))]
      (if (not-any? #(> (count %) 1) (vals merged))
        (into {} (map (fn [[k v]] [k (first v)]) merged))
        nil))
    nil))

(deftest test-merge-unique
  (test/is (= (merge-unique {:apple 1, :banana 5}
                            {:coconut 7})
              {:apple 1, :banana 5, :coconut 7}))
  (test/is (= (merge-unique {:apple 1, :banana 5}
                            {:apple 1, :banana 5, :coconut 7})
              {:apple 1, :banana 5, :coconut 7})
           "Both maps have :apple and :banana keys, but no conflicts.")
  (test/is (= (merge-unique {:apple 1, :banana 5} {:banana 6, :coconut 7})
              nil)
           "Both maps have a :banana key with different values")
  (test/is (= (merge-unique {:apple 1, :banana 5} {:banana 5, :coconut 7} {:coconut 7 :dates 8})
              {:apple 1, :banana 5, :coconut 7, :dates 8})
           "We can merge multiple maps"))

(defn pattern-variables
  "Extracts pattern variables from a given pattern"
  [pat]
  (cond
    (variable? pat)         #{pat}
    (expression? pat)       (reduce set/union (map pattern-variables pat))
    :else                   #{}))

(defn pattern?
  "Determines whether a given expression contains pattern variables"
  [expr] (not (empty? (pattern-variables expr))))

(declare pattern-match')

(defn- pattern-type
  "Decides the type of the pattern form."
  [bindings pattern]
  (let [pattern-vars (pattern-variables pattern)]
    (cond
      (empty? pattern-vars)                :constant  ;; Constant pattern
      (and (variable? pattern)
           (bindings pattern))             :bound-var  ;; Matched variables
      (and pattern-vars
           (not (variable? pattern)))      :free-pattern  ;; Non-variable pattern
      (seq-star-variable? pattern)         :free-star-variable
      (seq-plus-variable? pattern)         :free-plus-variable ;; Unbound sequence variable
      (variable? pattern)                  :free-variable  ;; Regular variable
      )))

(defn all-splits
  "Returns all ways to split a sequence in two"
  [coll]
  (for [n (range (inc (count coll)))] (split-at n coll)))

(deftest test-all-splits
  (test/is (= (all-splits '(1 2 3 4))
              [['() '(1 2 3 4)]
               ['(1) '(2 3 4)]
               ['(1 2) '(3 4)]
               ['(1 2 3) '(4)]
               ['(1 2 3 4) '()]]))
  (test/is (= (all-splits '())
              [[() ()]])))

(defn- match-exact-args
  "Helper function to match a sequence of patterns against a sequence of subjects, with
  order preserved.

  Returns a sequence of possible bindings."
  [bindings pattern-args subject-args]
  (if-let [pat (first pattern-args)]
    (let [match-and-continue (fn [[matched remainder]]
                               (match-exact-args
                                (merge-unique bindings {pat matched})
                                (rest pattern-args)
                                remainder))]
      (case (pattern-type bindings pat)
        :constant            (if (= pat (first subject-args))
                               (match-exact-args bindings
                                                 (rest pattern-args)
                                                 (rest subject-args))
                               nil)
        :bound-var           (if (= (bindings pat) (first subject-args))
                               (match-exact-args bindings
                                                 (rest pattern-args)
                                                 (rest subject-args)))
        :free-pattern        (if-let [matches (pattern-match' bindings pat (first subject-args))]
                               (let [rest-matches (match-exact-args bindings
                                                                    (rest pattern-args)
                                                                    (rest subject-args))]
                                 (->> (combo/cartesian-product matches rest-matches)
                                      (map #(reduce merge-unique %))
                                      (remove nil?)))) 
        :free-star-variable  (->> (all-splits subject-args)
                                  (mapcat match-and-continue)
                                  (remove nil?))
        :free-plus-variable  (->> (all-splits subject-args)
                                  rest
                                  (mapcat match-and-continue)
                                  (remove nil?))
        :free-variable       (if-let [subj (first subject-args)]
                               (match-and-continue [subj (rest subject-args)]))))
    (if (empty? subject-args)
      (list bindings)
      nil)))


(defn pattern-match-exact
  "Enables syntactic matching on a given expression. Order is preserved when matching arguments.

  Returns a list of possible bindings. Multiple bindings are possible as subexpressions may be 
  commutative, and sequence variables may match multiple combinations of arguments."
  [bindings pattern subject]
  (cond
    (variable? pattern)          (if-let [resolved (bindings pattern)]
                                   (if (pattern-match' {} resolved subject) (list bindings) nil)
                                   (if subject
                                     (list (assoc bindings pattern subject))))
    (expression? pattern)        (if (expression? subject)
                                   (match-exact-args bindings pattern subject)
                                   nil)
    :else                        (if (= pattern subject) (list bindings) nil)))


(defn- pattern-type-order [bindings elem]
  (case (pattern-type bindings elem)
    :constant               0
    :bound-var              1
    :free-pattern           2
    :free-variable          3
    :free-star-variable     4
    :free-plus-variable     4))


(defn- bound-to [bindings elem]
  (cond
    (variable? elem) (bindings elem)
    (empty? (pattern-variables elem)) elem
    :else nil))


(defn find-all-matches
  "Returns a sequence of triples of [matched, elem, remainder], where

  - matched is the first non-nil result of (func elem);
  - elem is an element of coll; and
  - remainder is a sequence of the remaining elements in coll. 

  If no element is found, returns empty list.
  
  Note: behaviour is not guaranteed if coll contains nil values!"
  [func coll]
  (loop [visited   (empty coll)
         current   (first coll)
         remainder (rest coll)
         accum     (list)]
    (if (nil? current) 
      accum
      (if-let [maybe-match (func current)]
        (recur (conj visited current)
               (first remainder)
               (rest remainder)
               (conj accum [maybe-match current (into visited remainder)]))
        (cond
          (empty? remainder) accum
          :else              (recur (conj visited current)
                                    (first remainder)
                                    (rest remainder)
                                    accum))))))
(find-all-matches even? '(1 2 3 4 5 6 7 8 9))
(defn find-first-match 
  "Returns a pair of [matched, remainder], where matched is the first non-nil result of
  (func elem), where elem is an element of coll; and remainder is a sequence of the remaining
  elements in coll. 

  If no element is found, matched is nil"
  [func coll]
  (first (find-all-matches func coll)))


(deftest test-find-match
  (test/is (= (find-first-match even? #{1 3 5 7 8 9 10})))
  (find-first-match even? '(1 3 5 7 8 9 10))
  (find-first-match even? #{1 3 5 7 9})
  (find-first-match even? '(1 3 5 7 9))
  (find-first-match even? [])
  (find-first-match even? #{}))


(defn all-partitions
  "This returns all ways to partition a given sequence into two, ignoring order."
  [coll]
  (concat 
   [(list coll [])]
   [(list [] coll)]
   (->> (combo/partitions coll :min 2 :max 2)
        (mapcat (fn [[x y]] (list [x y] [y x]))))))

(all-partitions '(1 2 3))
(defn match-commutative-args
  [bindings pattern-args subject-args]
  (if-let [pattern (first pattern-args)]
    (let [find-and-continue (fn [pred]
                              (let [[_ matched rem] (find-first-match pred subject-args)]
                                (if matched
                                  (->> (match-commutative-args bindings (rest pattern-args) rem)
                                       (map #(merge-unique bindings %))
                                       (remove nil?)))))
          bind-and-continue (fn [pattern matched remainder]
                              (->> remainder
                                   (match-commutative-args (merge-unique bindings matched)
                                                           (rest pattern-args))
                                   (remove nil?)))]
      (case (pattern-type bindings pattern)
        :constant           (find-and-continue #(= pattern %)) 
        :bound-var          (find-and-continue #(pattern-match' bindings (bindings pattern) %)) 
        :free-pattern       (->> subject-args
                                 (find-all-matches #(pattern-match' bindings pattern %))
                                 (mapcat (fn [[results _ rem]]
                                           (mapcat #(bind-and-continue pattern % rem) results))))
        :free-variable      (->> subject-args
                                 (find-all-matches #(pattern-match' bindings pattern %))
                                 (mapcat (fn [[results _ rem]]
                                           (mapcat #(bind-and-continue pattern % rem) results))))
        :free-star-variable (->> subject-args
                                 all-partitions
                                 (mapcat (fn [[matched rem]]
                                           (bind-and-continue pattern {pattern matched} rem))))
        :free-plus-variable (->> subject-args
                                 all-partitions
                                 (mapcat (fn [[matched rem]]
                                           (bind-and-continue pattern {pattern matched} rem))))))
    (if (empty? subject-args)
      (list bindings)
      nil)))
;; (match-commutative-args {'?x 5} '(+ 1 2 ) '(+ 1 2 ))
;; (match-commutative-args {} '(+ 7 1 ?&*) '(+ 1 2 3 4 5 7))
;; (match-commutative-args {} '(+ (- ?x 2) 7 ?x ?&*) '(+ (- 1 2) 7 1 2 3))
;; (match-commutative-args {} '(+ ?x ?&*) '(+ 1 2 3 4 5 6))

(defn pattern-match-commutative
  "Enables commutative matching on expressions. Conceptually, that means matching without
  regard to argument order.

  Example: (+ ?x 100) can match (+ 100 2) with ?x bound to 2.

  This is based on the ideas outlined in this paper: https://arxiv.org/pdf/1705.00907.pdf"
  [bindings pattern subject]
  (cond
    (variable? pattern)           (if-let [resolved (bindings pattern)]
                                    (if (pattern-match' {} resolved subject)
                                      (list bindings)
                                      nil)
                                    (list (assoc bindings pattern subject)))
    (and (expression? pattern)
         (expression? subject))   (let [type-order-sorter (partial pattern-type-order bindings)
                                        pattern-args      (sort-by type-order-sorter pattern)]
                                    (match-commutative-args bindings
                                                            pattern-args
                                                            subject))
    :else        (if (= pattern subject) (list bindings) nil)))


(defn pattern-match'
  [bindings pattern subject]
  (not-empty
   (if (commutative? (op-of pattern))
     (pattern-match-commutative bindings pattern subject)
     (pattern-match-exact bindings pattern subject))))


(defn match-all
  ([pattern subject] (pattern-match' {} pattern subject))
  ([pattern subject guard]
   ;; TODO: guards should be propagated into the matcher, to allow for early exits
   (->> (pattern-match' {} pattern subject)
        (filter guard))))

(defn match-one
  ;; TODO: guards should be propagated into the matcher, to allow for early exits
  ([pattern subject] (first (match-all pattern subject)))
  ([pattern subject guard] (first (match-all pattern subject guard))))


(match-all '(+ ?&* ?&*2) '(+ 1 2 3 4))
(deftest test-pattern-match-one
  (test/is (= (match-one '(+ ?x 1) '(+ 5 1))
              {'?x 5}))
  (test/is (= (match-one '(+ ?x 7) '(+ 5 1))
              nil))
  (test/is (= (match-one '?x '(- 5 6))
              {'?x '(- 5 6)}))
  (test/is (= (match-one '(- ?x ?y) '(- 1))
              nil))
  (test/is (= (match-one '(- ?x ?x) '(- 5 5))
              {'?x 5}))
  (test/is (= (match-one '(- ?x ?x) '(- 5 7))
              nil))
  (test/is (= (match-one '(+ 5 ?y 1 2 7) '(+ 2 1 5 7 2))
              {'?y 2}))
  (test/is (= (match-one '(+ 5 ?y) '(+ 24 5))
              {'?y 24})))

(deftest test-commutative-pattern-match
  (test/is (= (match-one '(* ?x 5) '(* 5 6))
              {'?x 6}))
  (test/is (= (match-one '(+ ?x 5) '(+ 5 6))
              {'?x 6}))
  (test/is (= (match-one '(+ ?x ?x) '(+ (* 2 x) (* 3 x)))
              nil))
  (test/is (= (match-one '(+ ?x (* ?y ?x)) '(+ 5 (* 6 5)))
              {'?x 5, '?y 6}))
  (test/is (= (match-one '(+ (- 5 ?y) 1 2) '(+ 2 1 (- 5 6)))
              {'?y 6}))
  (test/is (= (match-one '(+ 5 ?y) 24)
              nil))
  (test/is (let [matched (match-one '(+ ?x 10 (+ ?x))
                                    '(+ (+ 10 2) 10 (+ (+ 2 10))))]
             (or (= matched {'?x '(+ 10 2)})
                 (= matched {'?x '(+ 2 10)})))
           "?x should match (+ 10 2) or (+ 2 10)")
  (test/is (= (match-one '(+ 1 2 3) '(+ 3 1 2))
              {})
           "This is a valid match, and so should return an empty map (instead of nil)")
  (test/is (= (match-one '(+ 1 2 3 1) '(+ 3 1 2))
              nil)
           "This is not a valid match, and so should return nil"))


(deftest test-star-sequence-variables
  (test/is (= (-> (match-one '(* ?&*) '(* 1 2 3 4 5))
                  (get '?&*)
                  set)
              #{1 2 3 4 5})
           "Sequence variables should match a series of args.
            Multiply is commutative, so sequence order is not guaranteed")
  (test/is (= (match-one '(- ?&*) '(- 1 2))
              {'?&* '(1 2)})
           "Minus isn't commutative, so sequence variables should preserve order")
  (test/is (= (match-one '(+ ?&*) '(- 1 2))
              nil)
           "Constant patterns still need to be matched for sequence variables to match")
  (test/is (= (-> (match-one '(+ ?&* 5 7 9) '(+ 1 1 3 3 5 5 7 7 9 9))
                  (get '?&*)
                  sort)
              '(1 1 3 3 5 7 9))
           "Constant patterns still need to be matched for sequence variables to match"))

(test/run-tests)
(comment
  "Benchmark code"
  (time (match-one '(+ ?x (* ?x ?y)) '(+ 5 (* 5 6))))
  (time (match-one '(+ ?x (- 5 6)) '(+ (- 5 6) 100)))
  (time (match-one '(+ ?x (* ?x ?y)) '(+ 5 (* 6 5))))
  (time (match-one '(+ 1 2 ?x) '(+ 1 2 3)))
  ;; Benchmark with guard
  (time (match-one '(+ ?x ?y) '(+ 5 6) (fn [bindings]
                                         (let [?x (bindings '?x)] (= ?x 5)))))
  ;; Benchmark sequence variables
  (time (match-all
         '(+ 5 (+ 6 ?&* 10 ?&*2))
         '(+ 5 (+ 6 7 8 9 10))))
  (criterium/bench (match-one
                    '(= 0 (+ (* ?a (** ?x 2)) (* ?b ?x) ?c))
                    '(= (+ (* 7 y) (* 5 (** y 2)) (+ 7 8)) 0))))
